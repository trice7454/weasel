//
//  main.m
//  SimpleWeasel
//
//  Created by Terry Rice on 12/15/12.
//  Copyright (c) 2012 Terry Rice. All rights reserved.
//

#import <Foundation/Foundation.h>

int getScore(NSString *stringToMatch, NSString *stringToCompare);
NSString *createRandomString(NSString *lettersToUse, int lengthOfRandomSelection);
NSString *mutateString(NSMutableString *stringToMutate, NSString *lettersToUse, NSString *goalString);

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSString *stringToMatch = @"METHINKS IT IS LIKE A WEASEL";
        NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
        
        int numberOfIterationsPerCycle = 100;
        int targetScore = 28;
        int score = 0;
        
        // Create the first ancester
        NSMutableString *randomSelection = [[NSMutableString alloc] init];
        [randomSelection appendString:createRandomString(letters, (int)[stringToMatch length])];
        
        while (score < targetScore) {
            for (int i=0; i<numberOfIterationsPerCycle; i++) {
                NSString *tmpString = mutateString(randomSelection, letters, stringToMatch);
                int tmpScore = getScore(stringToMatch, tmpString);
                if (tmpScore > score) {
                    [randomSelection setString:tmpString];
                    score = tmpScore;
                }
                
                NSLog(@"%@ -- score %i", randomSelection, score);
                
                if (score == targetScore) {
                    break;
                }
            }
        }        
    }
    return 0;
}

// Get a score for the string to see where it falls
int getScore(NSString *stringToMatch, NSString *stringToCompare)
{
    const int MIN_INDEX = 0;
    int score = 0;
    
    for (int i = MIN_INDEX; i < [stringToMatch length]; i++) {
        if ([stringToMatch characterAtIndex:i] == [stringToCompare characterAtIndex:i]) {
            score++;
        }
    }
    
    return score;
}

// Create the initial random string;
NSString *createRandomString(NSString *lettersToUse, int lengthOfRandomSelection)
{
    NSMutableString *randomSelection = [[NSMutableString alloc] init];
    
    for (int j=0; j<lengthOfRandomSelection; j++) {
        // Pick the random letter
        int randomLetterPos = arc4random() % [lettersToUse length];
        [randomSelection appendFormat:@"%c", [lettersToUse characterAtIndex:randomLetterPos]];
    }
    
    return randomSelection;
}

// Mutate the string
// each position has a 5% chance of changing
// if the position already matches the goal it will not be changed
NSString *mutateString(NSMutableString *stringToMutate, NSString *lettersToUse, NSString *goalString)
{
    const int PERCENT_CHANCE = 5;
    const int HUNDRED_PERCENT = 100;
    const int ONE_PERCENT = 1;
    const int MIN_LETTER_INDEX = 0;
    const int MAX_LETTER_INDEX = 26;
    
    
    NSMutableString *mutatedString = [[NSMutableString alloc] init];

    for (int i=MIN_LETTER_INDEX; i < [stringToMutate length]; i++) {
        if ([stringToMutate characterAtIndex:i] != [goalString characterAtIndex:i]) {
            int chance =  ONE_PERCENT + arc4random() % (HUNDRED_PERCENT - ONE_PERCENT + 1);
            if (chance <= PERCENT_CHANCE) {
                int randomLetterPos = MIN_LETTER_INDEX + arc4random() % (MAX_LETTER_INDEX - MIN_LETTER_INDEX + 1);
                [mutatedString appendFormat:@"%c", [lettersToUse characterAtIndex:randomLetterPos]];
            }
            else {
                [mutatedString appendFormat:@"%c", [stringToMutate characterAtIndex:i]];                
            }
        }
        else {
            [mutatedString appendFormat:@"%c", [stringToMutate characterAtIndex:i]];
        }
    }
    
    return mutatedString;
}

